title: Exhibition, conference, workshop (Zagreb, 27.05 – 06.06.2015)
---
author: librarian
---
pub_date: 2015-04-27
---
tags:

log
timeline
---
body:

**Gallery Nova**, [Teslina 7](http://www.openstreetmap.org/?mlat=45.81102&mlon=15.97630#map=19/45.81102/15.97630), Zagreb

27.05 - 13.06.2015 **exhibition** and reading room  
04.06 - 06.06.2015 **conference**  
06.06 - 07.06.2015 **workshop** with **Kenneth** **Goldsmith** *Wasting time on the Internet*  

>*A public library is:*  
> - **free access** to books for every member of society
> - library **catalog**
> - **librarian**

> With books ready to be shared, meticulously cataloged, *everyone* is a **librarian**. When *everyone* is **librarian**, **library** is *everywhere*.[^5]

The **Public Library** cares for and preserves collective memory of all challenges and struggles with artificially induced scarcities, forbidden knowledge and exclusive territories. The activation of the public library's memory holds the key to its survival, but also to something much more important: it holds the key to the struggle for joint management of joint resources.

## Exhibition and reading room

![by Ivan Kuharić](nova.jpg)

Exhibition in **Gallery Nova** is a follow-up of the current collaboration on the project Public Library by curatorial collective **WHW** and **Multimedia Institute - mi2**. The project has been so far presented at international exhibitions Dear Art in Ljubljana's Museum of Contemporary Art -- Metelkova and Calvert 22 in London, and at the exhibition [Really Useful Knowledge](http://www.memoryoftheworld.org/blog/2014/10/28/really-useful-knowledge/) in the Museum Reina Sofía in Madrid. The earlier theme of art as infrastructure and new version of the exhibitionPublic Library are followed by the exploration of the theme of activation of materials of tactical archives and open digital collections.

The exhibition in Stuttgart's [Kunstverein](http://www.memoryoftheworld.org/blog/2014/10/30/public-library-rethinking-the-infrastructures-of-knowledge-production/) in 2014 presented Public Library as a collection of collections, and the form of repertory (used in archive science to describe a collection) was taken as a basic narrative procedure. In Gallery Nova we will present tactical archives: [MayDay Rooms](http://maydayrooms.org/) from London; The [Ignorant Schoolmaster and His Committees](http://www.uciteljneznalica.org/) from Belgrade; open digital repositories [Library Genesis](http://libgen.org/); [aaaaarg.org](http://aaaaarg.org);Catalogue of Free Books ([K\_O\_K](http://www.memoryoftheworld.org/blog/2014/10/28/k_o_k/)); (Digitized) [Praxis](http://www.memoryoftheworld.org/blog/2014/10/28/praxis-digitized/); digitized work of [Midnight Notes](http://www.memoryoftheworld.org/blog/2015/05/27/midnight-notes-digitized/) Collective and [Textz.com](http://www.memoryoftheworld.org/blog/2014/10/28/textz-com/). A special activation emphasis will be on the digital repositories: [UbuWeb](http://www.memoryoftheworld.org/blog/2014/10/28/ubuweb/) and [Monoskop](http://www.memoryoftheworld.org/blog/2014/10/28/monoskop/). Using the resources of these two repositories, WHW curated a selection of artistic and documentary film and video works; texts; audio files and books.

During the exhibition Showroom will host a selection of books related to the question of libraries, development of information studies, media theory and theory of the commons, borrowed from Media Archive of Multimedia Institute.

## Conference

In the framework of the exhibition we are organizing a conference that brings together artists, hackers and activists who are building infrastructures for knowledge exchange and activation of library holdings in the attempt to change social reality. The conference will be in English.

Conference participants are the following: **Dušan Barok** (*Monoskop*); **Anthony Davies** & **Rosemary Grennan** (*MayDay Rooms*); **Kenneth Goldsmith**; **Sebastian Lütgert** & **Jan Gerber** (*Open Media Library*); **Alberto Manguel**; **Marcell Mars** & **Tomislav Medak** (*Multimedia Institute*); **María G. Perulero** & **Celia Gradín** (*Platoniq*); **Noa Treister** (*The Ignorant Schoolmaster and His Committees*); **Dubravka Sekulić**; **Femke Snelting** (*Mondothèque*); **Što, kako i za koga / WHW**.

## Workshop with Kenneth Goldsmith Wasting time on the Internet

**Kenneth Goldsmith** is an American poet, editor-founder of net archive for avant-garde art UbuWeb, lecturer of poetry and poetic practices at Pennsylvania University. Live without dead time. --- Situationist graffiti, Paris, May 1968.

We spend our lives in front of screens, mostly wasting time: checking social media, watching cat videos, chatting, and shopping. What if these activities --- clicking, SMSing, status-updating, and random surfing --- were used as raw material for creating compelling and emotional works of literature? Could we reconstruct our autobiography using only Facebook? Could we write a great novella by plundering our Twitter feed? Could we reframe the internet as the greatest poem ever written?

organizers: **What, How and for Whom/WHW** and **Multimedia Institute - mi2**
