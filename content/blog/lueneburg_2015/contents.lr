title: Public Library Workshop. Common Media Lab, Lüne­burg. April 17-19.
---
author: librarian
---
tags:

log
timeline
---
pub_date: 2015-04-15
---
body:


![](lbs.jpg)

**Venue:** [DCRL](http://cdc.leuphana.com/structure/digital-cultures-research-lab/), [Am Sande 5](http://osm.org/go/0G9tbOz1?m=) (second floor, [map](http://osm.org/go/0G9tbOz1?m=))


In Lüneburg at [DCRL](http://cdc.leuphana.com/structure/digital-cultures-research-lab/) , from April 17-20, the [Public Library](http://www.memoryoftheworld.org) will gather together software and hardware developers, amateur librarians and researchers working on the topics and issues of curation, cataloging, and sharing books; rethinking user interfaces for reading, editing, compiling digital books; workflows of digitization; durability, longetivity and robustness of today's (shadow) libraries.

Amateur librarians ([Dubravka Sekulić](http://monoskop.org/Dubravka_Sekuli%C4%87), [Anthony Iles](http://monoskop.org/Anthony_Iles), [Spideralex](http://areyoubeingserved.constantvzw.org/Calafou.xhtml), [Tomislav Medak](http://monoskop.org/Tomislav_Medak)) will continue to care about books and collections at [https://library.memoryoftheworld.org](http://https://library.memoryoftheworld.org). A collections like **KOK** (Katalog oslobođenih knjiga, en. Catalogue of Liberated Books)[^1], **The Midnight Notes**[^2], **feminism**, **[biopolitics](http://biopolitics.kom.uni.st)**, **[ecomonomics](https://economics.memoryoftheworld.org/)**, **New Left Review** etc.

[Bodó Balázs](http://www.warsystems.hu/) will present his research on digital shadow libraries ["in the post-scarcity era"](http://www.warsystems.hu/2014/11/24/bodo-b-2015-libraries-in-the-post-scarcity-era-in-porsdam-ed-copyrighting-creativity-creative-values-cultural-heritage-institutions-and-systems-of-intellectual-property-ashgate/), from [Gigapedia](http://en.wikipedia.org/wiki/Library.nu) to [Library Genesis](http://libgen.org). Developers ([Jan Gerber](http://monoskop.org/Jan_Gerber), [Sebastian Luetgert](http://monoskop.org/Sebastian_Luetgert), [Marcell Mars](http://monoskop.org/Marcell_Mars), [Robert Ochshorn](http://rmozone.com/), [Dany Qumsiyeh](http://qhex.org/), [Klemo Vladimir](http://ccl.fer.hr/wordpress/people/research-assistants/klemo-vladimir/)) will present (and develop further) their work on [Linear book scanner](http://linearbookscanner.org/)[^3], [Hyperopia](http://hyperopia.meta4.org/), [Open Media Library](http://openmedialibrary.com/), [Memory of the World](https://www.memoryoftheworld.org/blog/2014/10/28/calibre-lets-share-books/).

The three days of Public Library gathering will move, merge and convert from [format](http://en.wikipedia.org/wiki/Typesetting) to [format](http://en.wikipedia.org/wiki/Content_format), from [workshop](http://en.wikipedia.org/wiki/Training_workshop) to [hackathon](http://en.wikipedia.org/wiki/Hackathon) to [unconference](http://en.wikipedia.org/wiki/Unconference) to [presentation](http://en.wikipedia.org/wiki/Presentation) to [lecture](http://en.wikipedia.org/wiki/Lecture) to [boredom](http://en.wikipedia.org/wiki/Boredom) to [fun](http://en.wikipedia.org/wiki/Fun).

## Program

**Friday**, *April 17*:  
- **18:30** - *All participants*: **Introduction. Update on recent research and development. Plans for the hackathon/workshop.**
- **20:30** - *Dinner*

**Saturday**, *April 18*:  
- **11:30** - *Tomislav Medak and Marcell Mars*: **Exhibition and Conference Public Library in Zagreb (May/June); Hackathon in Labin (June)**
- **12:00** - *Bodó Balázs*: **Shadow libraries**
- **13:30** - *Lunch*
- **15:30** - *Spideralex, Dubravka Sekulić, Anthony Iles, Tomislav Medak*: **Workflow I: metadata, collections, repertoriums**
- **17:30** - *Jan Gerber, Sebastian Luetgert, Klemo Vladimir, Marcell Mars:* **Workflow II: data, networks, distributions**

**Sunday**, *April 19*:
- **11:30** - *Dany Qumsiyeh*: **Linear bookscanner**
- **12:30** - *Dany Qumsiyeh, Robert Ochshorn, Tomislav Medak, Marcell Mars:* **Workflow III: scanning**
- **14:30** - *Lunch*
- **16:30** - *Robert Ochshorn, Jan Gerber, Sebastian Luetgert*: **Workflow IV: read, reference**


[^1]: **KOK** (Katalog oslobođenih knjiga, en. Catalogue of Liberated Books) is a collection digitized using the DIY book scanner at MaMa in Zagreb. It was primarily created with the aim of making available literature in Marxism, humanities, labor history and political economy/economics produced during the period of socialist Yugoslavia.
[^2]: The Midnight Notes collective, which marked a coming together of various radical strands of US left politics, began publishing its journal in 1979.
[^3]: Yes, famous vacuum cleaner flipping pages...
