<table width="643" cellspacing="0" cellpadding="4">
<colgroup>
<col width="178" />
<col width="449" /> </colgroup>
<tbody>
<tr valign="top">
<td width="178">
<p style="text-align: right;"><em>Collection Title:</em></p>
</td>
<td width="449">
<p class="repertorium-title-western"><strong>Printing Out the Internet</strong></p>
</td>
</tr>
<tr valign="top">
<td width="178">
<p align="right"><i>Identification/URL:</i></p>
</td>
<td width="449">
<p class="repertorium-body-western"><a title="Printing Out the Internet" href="http://printingtheinternet.tumblr.com/" target="_blank">http://printingtheinternet.tumblr.com/</a></p>
</td>
</tr>
<tr valign="top">
<td width="178">
<p align="right"><i>Creator:</i></p>
</td>
<td width="449">
<p class="repertorium-body-western"><strong>Kenneth Goldsmith</strong></p>
</td>
</tr>
<tr valign="top">
<td width="178">
<p align="right"><i>Abstract:</i></p>
</td>
<td width="449">
<p class="repertorium-body-western">Printing Out the Internet is a collection of printed content from the Internet. In 2013 Kenneth Goldsmith issued a call for internet users to send printed content from the Internet to the gallery LABOR in Mexico City with the intended purpose to print out the entire Internet. By the end of the project more than 20,000 people had sent in printed contributions.</p>
<p>A year later at Kunsthalle Düsseldorf 18,592 scientific articles totaling more than 230,000 pages were printed out and exhibited. The collection was earlier uploaded to The Pirate Bay by a user named Greg Maxwell, as a free culture gesture in support of Aaron Swartz who was at that time prosecuted by the US Attorney General’s office for allegedly downloading approximately 4.8M articles from the JSTOR repository of scientific journals. All articles in Maxwell&#8217;s collection are from the Philosophical Transactions of the Royal Society and even being in public domain, at the time of download, are behind the JSTOR&#8217;s paywall.</p>
<p>By printing out and displaying 33GB of paper Goldsmith&#8217;s JSTOR Pirate Headquarters gives a glimpse into the immensity of Swartz’s vision.</td>
</tr>
<tr valign="top">
<td width="178">
<p align="right"><i>Acquisition Information:</i></p>
</td>
<td width="449">
<p class="repertorium-aquisition/conditions-western">JSTOR repository torrented by Greg Maxwell. Printing Out the Internet courtesy of Kenneth Goldsmith.</p>
</td>
</tr>
<tr valign="top">
<td width="178">
<p align="right"><i>Conditions Governing Access:</i></p>
</td>
<td width="449">
<p class="repertorium-aquisition/conditions-western">Available offline in the gallery. Everyone can send their printed contribution.</p>
</td>
</tr>
<tr valign="top">
<td width="178">
<p align="right"><i>Biographical/Historical Information:</i></p>
</td>
<td width="449">
<p class="repertorium-body-western">Kenneth Goldsmith is the author of ten books of poetry and the founding editor of the online archive UbuWeb. An hour-long documentary on his work, &#8220;Sucking on Words&#8221; premiered at the British Library in 2007. From 1996-2009, Goldsmith was the host of a weekly radio show on New York City&#8217;s WFMU. He teaches writing at The University of Pennsylvania, where he is a senior editor of PennSound, an online poetry archive. He held the The Anschutz Distinguished Fellow Professorship in American Studies at Princeton University for 2009-10 and received the Qwartz Electronic Music Award in Paris in 2009. In 2011, he co-edited, Against Expression: An Anthology of Conceptual Writing and published a book essays, Uncreative Writing: Managing Language in the Digital Age. In 2013, he was appointed the Museum of Modern Art&#8217;s first Poet Laureate.</p>
</td>
</tr>
<tr valign="top">
<td width="178">
<p align="right"><i>Subjects:</i></p>
</td>
<td width="449">
<p class="repertorium-subjects-western"><em>Aaron Swartz, civil disobedience, scientific publishing, commodification in science, paywalled archives</em></p>
</td>
</tr>
</tbody>
</table>
