import glob
import pathlib
import re
from html_table_parser import HTMLTableParser


for f in glob.glob("/tmp/repertoriums/htmls/*"):
    repertorium = ""
    p = HTMLTableParser()
    p.feed(open(f).read())
    for t in [tuple(j) for j in p.tables[0]]:
        if t[0] == "Collection Title:":
            repertorium += """title: {0}
---
author:
librarian
---
pub_date: 2015-05-27
---
tags:
repertorium
---
body:
""".format(t[1])
            repertorium += '<table class="tablerepertorium">'
            file_path = '/tmp/repertoriums/lektor/repertorium_{0}/'.format(t[1]).lower()
            for c in "’'():":
                file_path = file_path.replace(c, "")
            file_path = file_path.replace(" ", "_").replace(".", "_")
        repertorium += '<tr><td class="tabletitle">{0}</td><td class="tablebody">{1}</td></tr>'.format(t[0], t[1])
    repertorium += "</table>"
    pathlib.Path(file_path).mkdir(parents=True, exist_ok=True)
    with open(file_path + "contents.lr", "w") as f:
        f.write(repertorium)

